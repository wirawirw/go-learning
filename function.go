package main

import "fmt"

func main()  {
	funVoid()

	single:=funReturn()
	multi,multi2:=funMultiReturn()
	multiname,multiname2:=funMultiReturNamed()
	funasval:=funAsValue
	filter:=func(name string) string {
		if name=="anjing" {
			return "...."
		}	else {
			return name
		}
	}
	funasParam:=funAsParam("wira", filter)
	fmt.Println(single,multi,multi2,multiname,multiname2,variadic(1,10,9,19,29),funasval("funasvalue"),funasParam)
	
}

func funVoid()  {
	fmt.Println("THIS IS FUNCTION LESSON")
}

func funReturn() string {
	return "THIS IS SINGLE RETURN"
}

func funMultiReturn() (string,int) {
	return "wira",3
}

func funMultiReturNamed()(name string,age int)  {
	name = "krisna"
	age = 23

	return
}

func variadic(numbers ...int) int {
	total:=0
	for _, number := range numbers {
		total+=number
	}

	return total
}

func funAsValue(name string) string {
	return  name
}

func funAsParam(name string,filter func( string)string)string {
	val:=filter(name)
	return val
}