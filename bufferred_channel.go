package main

import "fmt"

func main()  {
	channel := make(chan string,3)
	fx:=func(a string){
		val:=0
		for i := 0; i < 100; i++ {
			val=i
		}
		channel<- string(val)+" Val"+a
	}
	go fx("A")
	go fx("B")
	go fx("C")


	fmt.Println(<-channel)
	fmt.Println(<-channel)
	fmt.Println(<-channel)
}

