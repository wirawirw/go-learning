package main

import "fmt"

func main()  {
	var numberA int = 3
	var numberB *int = &numberA

	fmt.Println(numberA)
	fmt.Println(&numberA)

	fmt.Println(numberB)
	fmt.Println(*numberB)

	numberA=4

	fmt.Println(numberA)
	fmt.Println(&numberA)

	fmt.Println(numberB)
	fmt.Println(*numberB)
}


