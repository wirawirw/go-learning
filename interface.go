package main

import "fmt"

type Geometry interface {
	HitungLuas()float32
}

type PersegiPanjang struct {
	panjang,lebar float32
}

func (persegi PersegiPanjang) HitungLuas() float32 {
	luas:= persegi.panjang*persegi.lebar
	return luas
}

type PersegiSamaSisi struct {
	panjang float32
}

func (value PersegiSamaSisi) HitungLuas()float32 {
	return value.panjang*value.panjang
}

func main()  {
	var geometry Geometry
	persegiPanjang:=PersegiPanjang{
		panjang: 20,
		lebar:   40,
	}

	geometry=persegiPanjang

	fmt.Println(geometry.HitungLuas())


	persegiSamaSisi:=PersegiSamaSisi{panjang: 30}
	geometry=persegiSamaSisi
	fmt.Println(geometry.HitungLuas())
}