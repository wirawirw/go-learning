package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup
func main()  {
	wg.Add(2)
	go loop("Hey")
	go loop("Dude")
	wg.Wait()

	wg.Add(1)
	go loop("dont make it bad")
	wg.Wait()
}

func loop(text string)  {
	for i := 0; i < 5; i++ {
		fmt.Println(text)
	}
	defer wg.Done()
}
