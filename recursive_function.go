package main

import "fmt"

func main()  {
	fac:=factorial(3)
	facres:=factorialRecursive(3)
	fmt.Println(fac,facres)
}

func factorial(value int)int  {
	result:=1
	for i := value; i >0; i-- {
		result*=i
	}

	return result
}

func factorialRecursive(value int)int  {
	if value==1 {
		return 1
	}else {

		return value*factorialRecursive(value-1)
	}
}