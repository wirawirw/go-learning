package main

import "fmt"

func getAverage(numbers []int, ch chan float64) {
	var sum = 0
	for _, e := range numbers {
		sum += e
	}
	ch <- float64(sum) / float64(len(numbers))
}

func getMax(numbers []int, ch chan int) {
	var max = numbers[0]
	for _, e := range numbers {
		if max < e {
			max = e
		}
	}
	ch <- max
}
func main()  {
	average:=make(chan float64)
	max:=make(chan int)
	numbers:=[]int{
		1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,
	}
	go getAverage(numbers,average)
	go getMax(numbers,max)

	for i := 0; i < 2; i++ {
		select {
		case avg := <-average:
			fmt.Printf("Avg \t: %.2f \n", avg)
		case max := <-max:
			fmt.Printf("Max \t: %d \n", max)
		}
	}


}

