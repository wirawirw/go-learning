package main

import "fmt"

func main()  {
	var  ex = map[string]int{
		"tiga":3,
		"dua":2,
		"satu":1,
	}

	book:=make(map[string]string)
	book["title"]="GOLEARN"
	book["author"]="wirnat"
	fmt.Println(book)

	fmt.Println(ex["satu"])
	name,test:=ex["empat"]
	fmt.Println(name,test)

	arrayMap()

}

func arrayMap()  {
	var nestle = []map[string]string{
		map[string]string{
			"bisnis":"manufaktur",
			"name":"urban",
			"skor":"8.7",
		},
		map[string]string{
			"bisnis":"fintech",
			"name":"marketbiz",
			"skor":"4.7",
		},
		map[string]string{
			"bisnis":"startup",
			"name":"island digital",
			"skor":"1.4",
		},
	}
	i:=0
	for i< len(nestle) {
		println(nestle[i]["bisnis"])
		i++
	}
}
