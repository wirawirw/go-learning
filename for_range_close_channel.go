package main

import "fmt"

func main()  {
	chx:=make(chan string)
	go sendMessage(chx)
	printMessage(chx)


}

func sendMessage(ch chan<- string) {
	for i := 0; i < 20; i++ {
		ch <- fmt.Sprintf("data %d", i)
	}
	close(ch)
}

func printMessage(ch <-chan string) {
	for message := range ch {
		fmt.Println(message)

	}
}