package main

import (
	"fmt"
	"strings"
)

func main()  {
	fmt.Println(dinamis("DINAMIS data"))
	fmt.Println(dinamis(32))
	fmt.Println(dinamis(1.5))
	caster()
	mapDinamis()
}

func caster()  {
	var dinamisVal interface{}
	dinamisVal=[]string{
		"satu",
		"dua",
		"tiga",
	}
	slice:=strings.Join(dinamisVal.([]string),", ")

	fmt.Println(slice[1])

	dinamisVal=3
	fmt.Println("intrface string to int",dinamisVal.(int)*10)
}

func mapDinamis()  {
	res:=map[string]interface{}{
		"name":"wira",
		"id":1,
		"address":"denpsar",
		"studied":[]string{
			"PNB","UNUD",
		},
	}

	mapdinamis:=[]interface{}{
		"oke",
		12,
		map[string]string{
			"test":"interface",
			"t":"ok",
		},
	}
	fmt.Println(mapdinamis)

	fmt.Println("interface string to array:",strings.Join(res["studied"].([]string), ", "))

}

func dinamis(value interface{}) interface{} {
	return value
}
