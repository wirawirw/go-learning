package main

import "fmt"

func main()  {
	process:=make(chan int)

	var sayHelloTo = func(text string) {
		for i := 0; i < 300; i++ {
			fmt.Println("A")
		}
		process<-100
	}

	var hitting = func() {
		for i := 0; i < 3; i++ {
			fmt.Println("B")
		}
		process<-1
	}

	go hitting()
	go sayHelloTo("wira,lets count")
	printing(process)
}

func printing(chx chan int)  {
	a:=<-chx
	b:=<-chx
	fmt.Println(a)
	fmt.Println(b)
}